<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Title</title>
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="form-style-2">
    <div class="form-style-2-heading">
        E-Library
    </div>
    <form method="POST" action="">
        <label for="name"> Name of book:
            <input class="input-field" type="text" name="name" id="name">
        </label>
        <label for="author">Author:
            <input class="input-field" type="text" name="author" id="author">
        </label>
        <label for="description">Description: </label>
        <textarea class="input-field" type="text" name="description" id="description" rows="10" cols="45"></textarea>
        <br>
        <br>
        <input class="input-field" type="submit" value="Add book">
    </form>

</div>
<div class="form-style-2">
    <div class="form-style-2-heading">
        Books registered!
    </div>
    <div class="form-style-2">
        <div class="form-style-2-heading">
            <table class="table-area"  border="1" cellpadding="5" cellspacing="1">
        <tr>
            <th> Book name </th>
            <th> author name </th>
            <th> description </th>
        </tr>
        <c:forEach items="${booksFromServer}" var="book">
            <tr>
                <td>${book.bookName}</td>
                <td>${book.authorName}</td>
                <td>${book.description}</td>
            </tr>
        </c:forEach>
     </table>
        </div>
    </div>
</div>
</body>
</html>