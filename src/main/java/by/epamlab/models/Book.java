package by.epamlab.models;

public class Book {

    private Integer id;
    private String bookName;
    private String authorName;
    private String description;

    public Book(){
    }

    public Book(Integer id, String bookName, String authorName, String description) {
        this.id = id;
        this.bookName = bookName;
        this.authorName = authorName;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }





}
