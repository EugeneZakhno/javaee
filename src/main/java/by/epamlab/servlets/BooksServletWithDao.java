package by.epamlab.servlets;

import by.epamlab.dao.BooksDao;
import by.epamlab.models.Book;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import by.epamlab.dao.BooksDaoJdbcImpl;
import java.lang.String;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;

@WebServlet ("/books")
public class BooksServletWithDao extends HttpServlet {

    private BooksDao booksDao;
    private Connection connection;

    @Override
    public void init() throws ServletException {
        Properties properties = new Properties();
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        try {
            properties.load(new FileInputStream(getServletContext().getRealPath("/WEB-INF/classes/db.properties")));
            String dbUrl = properties.getProperty("db.url");
            String dbBookName = properties.getProperty("db.book_name");
            String dbPassword = properties.getProperty("db.password");
            String driverClassName = properties.getProperty("db.driverClassName");

            dataSource.setUsername(dbBookName);
            dataSource.setPassword(dbPassword);
            dataSource.setUrl(dbUrl);
            dataSource.setDriverClassName(driverClassName);
            booksDao = new BooksDaoJdbcImpl(dataSource);

            Class.forName(driverClassName);
            connection = DriverManager.getConnection(dbUrl, dbBookName, dbPassword);

        } catch (IOException | ClassNotFoundException | SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String bookName = request.getParameter("name");
       String authorName = request.getParameter("author");
       String descriptionText = request.getParameter("description");

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO fix_book(book_name, author_name, description) VALUES (?,?,?)");
            preparedStatement.setString(1,bookName);
            preparedStatement.setString(2,authorName);
            preparedStatement.setString(3,descriptionText);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        response.sendRedirect("/books");
    }

    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws   ServletException, IOException {

        List <Book> books = booksDao.findAll();
        if (books == null) {
            if (books != null) {
                throw new UnavailableException("The page is not available");
                } else {
                throw new UnavailableException("Please. A little bit wait" + 2000, 2000);
            }
        }
        request.setAttribute("booksFromServer", books);
        request.getServletContext().getRequestDispatcher("/jsp/books.jsp").forward(request, response);
    }
}
