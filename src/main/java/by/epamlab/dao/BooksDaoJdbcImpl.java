package by.epamlab.dao;

import by.epamlab.models.Book;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BooksDaoJdbcImpl implements BooksDao {

    //language=SQL
    private final String SQL_SELECT_ALL = "SELECT * FROM fix_book";

    private Connection connection;

    public BooksDaoJdbcImpl(DataSource dataSource) {
        try {
            this.connection = dataSource.getConnection();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public List<Book> findAll() {

        try {
            List <Book> books = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String bookName = resultSet.getString("book_name");
                String authorName = resultSet.getString("author_name");
                String description = resultSet.getString("description");

                Book book = new Book(id, bookName, authorName, description);
                books.add(book);
            }
          return books;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

