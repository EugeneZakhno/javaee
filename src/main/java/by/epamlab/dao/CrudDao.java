package by.epamlab.dao;

import java.util.List;

public interface CrudDao <T>{

    List<T> findAll();

}
