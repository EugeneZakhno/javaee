import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import java.util.Map;
import java.util.logging.Logger;

public class InfoListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(InfoListener.class.getName());

    public InfoListener() {

    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        ServletContext context = servletContextEvent.getServletContext();

        LOGGER.info(context.getServletContextName());
        LOGGER.info(context.getServerInfo());

        Map<String, ? extends ServletRegistration> registrations = context.getServletRegistrations();
        for (Map.Entry<String, ? extends ServletRegistration> entry : registrations.entrySet()){
            LOGGER.info(entry.getKey() + ":" + entry.getValue().getName());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
